package com.dmytro_glynskyi.testtwowaybinding

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.math.BigInteger
import java.security.MessageDigest

class MyViewModel : ViewModel() {

    val items = MutableLiveData<List<Item>>()
    val hashCode = MutableLiveData<String>()

    init {
        items.value = (1..1000).map { index -> Item(index, "value-$index") }
        items.observeForever { items ->
            hashCode.value = items.map { it.value }.joinToString().md5()
        }
    }

    fun String.md5(): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
    }
}