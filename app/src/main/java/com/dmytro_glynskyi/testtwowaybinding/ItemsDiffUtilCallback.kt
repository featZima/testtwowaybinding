package com.dmytro_glynskyi.testtwowaybinding

import androidx.recyclerview.widget.DiffUtil

class ItemsDiffUtilCallback(
    private val oldValue: List<Item>,
    private val newValue: List<Item>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldValue[oldItemPosition] == newValue[newItemPosition]
    }

    override fun getOldListSize(): Int {
        return oldValue.size
    }

    override fun getNewListSize(): Int {
        return newValue.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}