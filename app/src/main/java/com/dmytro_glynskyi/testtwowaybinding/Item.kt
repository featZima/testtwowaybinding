package com.dmytro_glynskyi.testtwowaybinding

data class Item(
    val id: Int,
    val value: String
)
