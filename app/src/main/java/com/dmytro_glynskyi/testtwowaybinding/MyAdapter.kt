package com.dmytro_glynskyi.testtwowaybinding

import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item.view.*

class MyAdapter : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    var items = listOf<Item>()
        set(newValue) {
            val oldValue = field
            field = newValue
            DiffUtil
                .calculateDiff(ItemsDiffUtilCallback(oldValue, newValue))
                .dispatchUpdatesTo(this)
        }
    var listener: (() -> Unit)? = null

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    private fun updateItem(id: Int, newValue: String) {
        items = items.map { item ->
            if (item.id == id) item.copy(value = newValue)
            else item
        }
        listener?.invoke()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Item) {
            (itemView.itemEditText.tag as? TextWatcher)?.let(itemView.itemEditText::removeTextChangedListener)
            val textWatcher = onTextChanged { updateItem(item.id, it) }
            if (itemView.itemEditText.text.toString() != item.value) {
                itemView.itemEditText.setText(item.value)
            }
            itemView.itemEditText.tag = textWatcher
            itemView.itemEditText.addTextChangedListener(textWatcher)
        }
    }
}