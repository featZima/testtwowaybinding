package com.dmytro_glynskyi.testtwowaybinding

import android.text.Editable
import android.text.TextWatcher

fun onTextChanged(block: (String) -> Unit): TextWatcher {
    return object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            block(s.toString())
        }
    }
}