package com.dmytro_glynskyi.testtwowaybinding

import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.recyclerview.widget.RecyclerView

object RecyclerViewBindings {

    @JvmStatic
    @BindingAdapter(value = ["items"])
    fun bindItems(view: RecyclerView, items: List<Item>?) {
        items?.let {
            (view.adapter as MyAdapter).items = it
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["itemsAttrChanged"])
    fun bindItemsAttrChanged(recyclerView: RecyclerView, listener: InverseBindingListener) {
        @Suppress("UNCHECKED_CAST")
        (recyclerView.adapter as MyAdapter).listener = { listener.onChange() }
    }

    @JvmStatic
    @InverseBindingAdapter(attribute = "items")
    fun getSelectionMode(recyclerView: RecyclerView): List<Item> {
        @Suppress("UNCHECKED_CAST")
        return (recyclerView.adapter as MyAdapter).items
    }
}